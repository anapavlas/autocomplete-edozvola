var autocompleteOptions = [
   { id: 1, value: "Train 1"},
   { id: 2, value: "Train 2"},
   { id: 3, value: "Train 3"},
   { id: 4, value: "Train 4"},
   { id: 5, value: "Train 5"},
   { id: 6, value: "Train 6"},
   { id: 7, value: "Train 7"},
   { id: 8, value: "Train 8"},
   { id: 9, value: "Train 9"},
   { id: 10, value: "Train 10"}
];

var autocompleteOptions2 = [
   { id: 1, value: "Vagon Hrvatska"},
   { id: 2, value: "Vagon Hungary"},
   { id: 3, value: "Train 3"},
   { id: 4, value: "Train 4"},
   { id: 5, value: "Train 5"},
   { id: 6, value: "Train 6"},
   { id: 7, value: "Train 7"},
   { id: 8, value: "Train 8"},
   { id: 9, value: "Train 9"},
   { id: 10, value: "Train 10"}
];

var autocompleteDummyList = [
    { id: 3, value: "Train 3"},
    { id: 4, value: "Train 4"},
    { id: 5, value: "Train 5"},
    { id: 6, value: "Train 6"}
];

function autocompleteCallback(itemId, autocompleteElement) {
    
    var textareaField = autocompleteElement != null ? autocompleteElement : $("li").parents(".autocompleteDropdownWrapper").prev();
    var vagonInput = $("#inputKojiseSamopupunjava");
    
    $(vagonInput).val("Vagon");
}


function autoResizeTextarea(element) {
    
          $(element).each(function (i, elem) {
    
              var offset = elem.offsetHeight - elem.clientHeight;
    
              var resizeTextarea = function(el) {
                  $(el).css('height', 'auto').css('height', el.scrollHeight + offset);
              };
    
              var setCaretHeight = function(el) {
                  var elemHeight = $(el).parents('.form-group').outerHeight();
                  var hasError = $(el).siblings('.form-control-error-message').length > 0;
    
                  if (!hasError) {
                    $(el).siblings('.caret-component').css('line-height', elemHeight + 'px');
                  } else {
                    var errorHeight = $(el).siblings('.form-control-error-message').outerHeight();
                    var elemHeight = elemHeight - errorHeight - 10;
                    $(el).siblings('.caret-component').css('line-height', elemHeight + 'px');
                  }
              }
    
              resizeTextarea(elem);
              setCaretHeight(elem);
    
              $(elem).on('keyup paste change input focus click', function() {
                  resizeTextarea(this);
                  setCaretHeight(this);
              });
    
          });
    }
    
    
    //Use this function to replace potential characters that could break the regex
    RegExp.escape = function (s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    };
    
    // funkcija koja radi autocomplete magiju
    function autocomplete(elem, event, page) {
        // ako postoji već lista od prethodnog autocompletea - makni ju
        removeAutocompleteDropdown();
    
        // izvlačimo iz data atributa elemenata
        var optionsResource = autocompleteDummyList;
        var optionsAjaxLoad = $(elem).attr("data-autocomplete-ajax-load") == "true";
        var idKey = $(elem).attr("data-autocomplete-id");
        var valueKey = $(elem).attr("data-autocomplete-value");
        var allowedFreeInput = $(elem).attr("data-allowed-free-input") == "true";
    
        // pagination
        var paginate = $(elem).attr("data-autocomplete-paginate") == "true";
        var itemsPerPage = $(elem).attr("data-autocomplete-items-per-page") ? $(elem).attr("data-autocomplete-items-per-page") : 10;
        var pageNumber = page ? page : 0;
    
        // input value
        var inputValue = event.type == "click" ? "" : $(elem).val();
    
        if (optionsAjaxLoad) {
            optionsResource(inputValue, pageNumber, itemsPerPage, function(data) {
                showAutocompleteDropdown(elem, event, data.content, idKey, valueKey, paginate, pageNumber, itemsPerPage, data.totalElements, allowedFreeInput);
            });
        } else {
            var items = [];
            if (inputValue) {
                for (var i = 0; i < optionsResource.length; i++) {
                    var testableRegExp = new RegExp(RegExp.escape(inputValue), "i");
                    var autocompleteItem = optionsResource[i];
                    if (autocompleteItem[valueKey].match(testableRegExp)) {
                        items.push(autocompleteItem);
                    }
                }
            } else {
                items = optionsResource;
            }
    
            if (paginate) {
                items = items.slice(pageNumber, itemsPerPage);
            }
    
            showAutocompleteDropdown(elem, event, items, idKey, valueKey, paginate, pageNumber, itemsPerPage, items.length, allowedFreeInput); // todo ispraviti
        }
    }
    
    function showAutocompleteDropdown(elem, event, items, idKey, valueKey, paginate, pageNumber, itemsPerPage, itemsTotal, allowedFreeInput) {
    
        // pripremi jedan element za prikaz autocomplete vrijednosti
        var elemDropdown = $('<div class="autocompleteDropdownWrapper" data-select-first-onblur=' + !allowedFreeInput + ' style="display: none;"><ul class="autocomplete-dropdown"></ul></div>');
    
        //var textareaWidth = $(".autocompleteDropdownWrapper").prev().outerWidth();
        var textareaWidth = $(elem).outerWidth();
        //$(".autocompleteDropdownWrapper", $(elem).parent()).css("width", textareaWidth);
        $(elemDropdown).css("width", textareaWidth);
    
        // dodaj taj element elementu u kojem se nalazi autocomplete input
        $(elem).parent().append(elemDropdown);
    
        // popuni dropdown vrijednostima
        for (var i = 0; i < items.length; i++) {
            var autocompleteItem = items[i];
            $('ul', elemDropdown).append('<li data-id='+autocompleteItem[idKey]+'>'+autocompleteItem[valueKey]+'</li>');
        }
    
        // ako imamo pagination i ako je on potreban
        if (paginate && itemsTotal > itemsPerPage) {
            // element za pagination kontrole
            var paginationControls = $('<div class=controls></div>');
            var numberOfPages = Math.ceil(itemsTotal / itemsPerPage);
    
            // build navigation html
            var paginationNavigation = '';
            if (pageNumber > 0) {
                paginationNavigation += '<a class="prev" data-page-nr="' + (pageNumber-1) +'"><i class="fa fa-angle-left"></i></a>';
            }
            // odkomentirati ako će trebati brojčani prikaz stranica u paginationu
            /*var currentPage = 0;
            while (numberOfPages > currentPage) {
                paginationNavigation += '<a class="page ' + ((pageNumber == currentPage) ? 'active' : '')
                    + '" data-page-nr="' + currentPage + '" longdesc="' + currentPage + '">' + (currentPage + 1) + '</a>';
                currentPage++;
            }*/
            if (pageNumber < numberOfPages) {
                paginationNavigation += '<a class="next" data-page-nr="' + (pageNumber+1) +'"><i class="fa fa-angle-right"></i></a>';
            }
    
            $(paginationControls).append(paginationNavigation);
    
            // dodaj handlere za pagination
            $("a.prev, a.page, a.next", $(paginationControls)).click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                autocomplete(elem, event, $(this).data("page-nr"));
            });
    
            $(elemDropdown).append(paginationControls);
        }
    
        // u ovom trenutku svu sve opcije pripremljene i select element je pripremljen - hajdemo nakačiti handlere na optione
        $('ul.autocomplete-dropdown li', elemDropdown).click(function(evt) {
            // kada se odabere vrijednost - iz odabranog optiona uzimamo text
            var selectedValueText = $(this).text();
            var selectedValueId = $(this).attr("data-id");
    
            if($(elem).attr("data-autocomplete-callback")) {
                window[$(elem).attr("data-autocomplete-callback")](selectedValueId);
            }
    
            // i postavljamo na input
            $(elem).val(selectedValueText);
            autoResizeTextarea("textarea");
    
            // sad nam više taj select s optionima ne treba pa ga brišemo
            removeAutocompleteDropdown();
        });
    
        // prikaži dropdown listu
        $(elemDropdown).show();
    
    }
    
    function clearAutocompleteValue(elem) {
        if ($(elem).attr("data-autocomplete-callback") && $(elem).attr("data-autocomplete-callback").length > 0) {
            window[$(elem).attr("data-autocomplete-callback")](null, $(elem));
        }
        $(elem).val("");
    }
    
    function removeAutocompleteDropdown(elem) {
        if (elem) {
            $(".autocompleteDropdownWrapper", $(elem).parent()).remove();
        } else {
            $(".autocompleteDropdownWrapper").remove();
        }
    }
    
    function initAutocomplete (selector) {
        var autocompleteKeyupTimers = {};
    
        $(selector).each(function(i, elem) {
    
            autocompleteKeyupTimers[$(elem).attr("id")] = null;
    
            // na svaki autocomplete element attachaj keyup event...
            $(elem).unbind("keyup").on("keyup", function(e) {
    
                // obustavi zadnji timer
                if (autocompleteKeyupTimers[$(elem).attr("id")]) {
                    clearTimeout(autocompleteKeyupTimers[$(elem).attr("id")]);
                }
    
                // ako smo obrisali sve iz inputa, počisti autocomplete
                if ($(this).val().length == 0) {
                    clearAutocompleteValue($(this));
                    removeAutocompleteDropdown();
                } else {
                    autocompleteKeyupTimers[$(elem).attr("id")] = setTimeout(autocomplete(elem, e), 350);
                }
    
                // klikom na textarea prekini propagiranje eventa na paginaciji kako se dropdown ne bi removeao
                $(".controls").click(function(e) {
                   e.stopPropagation();
                });
            });
    
            var caret = $(elem).siblings('.caret-component');
            // i klik event
            $(caret).unbind("click").on("click", function(e) {
                autocomplete(elem, e);
                e.stopPropagation();
                // klikom na textarea prekini propagiranje eventa na paginaciji kako se dropdown ne bi removeao
                $(".controls").click(function(e) {
                   e.stopPropagation();
                });
            });
    
            // hendlamo slučaj kada autocomplete nema dozvoljen slobodan unos a trenutno unesena vrijednost ne nudi nijednu opciju u dropdownu.
            // U ovom slučaju želimo resetirati unos i pozvati callback (ako ga ima) s vrijednošću null (zato da callback odradi što treba i evenutalno
            // resetira i vezana polja.
            $(elem).unbind("blur").on("blur", function(e) {
                if (($(this).attr("data-allowed-free-input") && $(this).attr("data-allowed-free-input") != "true") && $('.autocompleteDropdownWrapper li').length == 0) {
                    clearAutocompleteValue($(this));
                }
            });
        });
    }
    
    $(document).ready(function () {
    
        // klikom na bilokoji dio dokumenta autocomplete dropdown se zatvara
        $('*').not('.caret-component, .autocomplete-dropdown li').on("click", function() {
    
            // TODO ovaj if statement treba malo bolje složiti. matko
    
            // ako uopće imamo dropdown s vrijednostima
            if ($('.autocompleteDropdownWrapper').length > 0) {
                // ako filter vraća neki set vrijednosti, na blur autocomplete-a triggeriraj klik na prvu vrijednost iz dropdown-a ako je to autocomplete
                // u kojem moramo odabrati neku vrijednost (freeinput == false)
                var autocompleteElement = $('.autocompleteDropdownWrapper').siblings('.autocomplete');
                var selectFirstOnblurAndHasValue = $('.autocompleteDropdownWrapper[data-select-first-onblur="true"] li').length > 0 && $(autocompleteElement).val().length > 0;
                if (selectFirstOnblurAndHasValue) {
                    $('.autocompleteDropdownWrapper[data-select-first-onblur="true"] li:first-child').trigger('click');
                } else {
                    if (($(autocompleteElement).attr("data-allowed-free-input") && $(autocompleteElement).attr("data-allowed-free-input") != "true") && $('.autocompleteDropdownWrapper li').length == 0) {
                        clearAutocompleteValue($(autocompleteElement));
                    }
                }
            }
    
            removeAutocompleteDropdown();
    
        });
    
        initAutocomplete(".autocomplete:not([readonly])");
    });
    //# sourceURL=autocomplete.js