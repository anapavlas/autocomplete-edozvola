# Autocomplete dropdown

Funkcionalnost autocomplete dropdown izbornika sastoji se od nekoliko komponenti spojene u jednu
* Textarea koji vizualno izgleda i ponaša se kao padajući izbornik 
* Nudi filtriranje i autocomplete funkcionalnost nad 'padajućim izbornikom'
* Nudi mogućnost slobodnog unosa u polje
* Omogućuje paginaciju na izbornicima koji dohvaćaju veliki set podataka
* Omogućuje predodabir polja koje je povezano s autocomplete poljem

## Tipovi autocomplete izbornika
* Padajući izbornik s autocomplete-om bez slobodnog unosa
* Padajući izbornik s autocomplete-om i slobodnim unosom teksta
* Padajući izbornik s paginacijom koji predpopunjava drugo input polje


